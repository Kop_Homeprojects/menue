"use strict";

let braces = 0;
document.getElementById("display").value = "";
let expression = "";
let rad = false;
let secondND = false;
let pressed = false;

document.addEventListener("DOMContentLoaded", addEventListeners)

function addEventListeners() {
    document.getElementById("advanced-function1").addEventListener("click", secondOperations);
    document.getElementById("advanced-function2").addEventListener("click", setToRad);
    document.getElementById("advanced-function3").addEventListener("click", () => {
        addToDisplay(document.getElementById("advanced-function3").value);
    });
    document.getElementById("percent").addEventListener("click", () => {
        addToDisplay(document.getElementById("percent").value)
    })
    document.getElementById("clearButton").addEventListener("click", clearDisplay);
    document.getElementById("braces").addEventListener("click", () => {
        addBraces(document.getElementById("braces").value);
    });
    setAdvancedFunctionsEventListener();
    document.getElementById(".").addEventListener("click", () => {
        addToDisplay(".");
    })
    document.getElementById("calculate").addEventListener("click", calculate);
    for (let i = 0; i <= 9; i++) {
        document.getElementById("" + i).addEventListener("click", () => {
            addToDisplay(i);
        })
    }
    for (let i = 1; i <= 4; i++) {
        document.getElementById("operators" + i).addEventListener("click", () => {
            addToDisplay(document.getElementById("operators" + i).value);
        })
    }
}

function setAdvancedFunctionsEventListener() {
    for (let i = 4; i <= 15; i++) {
        document.getElementById("advanced-function" + i).addEventListener("click", () => {
            addToDisplay(document.getElementById("advanced-function" + i).value);
        });
    }
}

function revealAdvancedCalculator() {
    let classname;
    if (document.getElementById("advanced-function1").className == "advanced-functions-activated") {
        classname = "button operator advanced"
        document.getElementsByClassName("calculator").item(0).style.maxWidth = "400px";
    } else {
        classname = "advanced-functions-activated";
        document.getElementsByClassName("calculator").item(0).style.maxWidth = "400px";
    }
    for (let i = 1; i < 16; i++) {
        document.getElementById("advanced-function" + i).setAttribute("class", classname);
    }

}

function addMultiplicationSign(text) {
    if (!["+", "-", "*", "/"].includes(text.charAt(text.length - 1)) && text.length >= 1) {
        text += "*";
        expression = expression += "*";
    }
    return text;
}

function addBraces() {
    let text = document.getElementById("display").value;
    if (braces == 0) {
        text = addMultiplicationSign(text);
        document.getElementById("display").value = text + "(";
        expression = expression += "(";
        braces++;
    } else if (text.length > 0) {
        document.getElementById("display").value = text + ")";
        expression = expression += ")";
        braces--;
    }
    formatBraces();
}

function addToDisplay(value) {
    let display = document.getElementById("display");
    value = value.toString();
    let currentValue = display.value;
    if (["+", "-", "*", "/"].includes(value) || value == ".") {
        if (["+", "-", "*", "/"].includes(currentValue.charAt(currentValue.length - 1)) && currentValue.length > 1) {
            display.value = display.value.substring(0, display.value.length - 1) + value;
            expression = expression.substring(0, display.value.length-1)+value;
            pressed = false;
        } else if (!["+", "-", "*", "/"].includes(currentValue.charAt(currentValue.length - 1)) && currentValue.length >= 1 && value == "." && !pressed) {
            display.value = display.value + ".";
            expression = expression + ".";
            pressed = !pressed;
        } else if (value != ".") {
            expression = expression += value;
            display.value += value;
            pressed = false;
        }
    } else if (value == "+/-") {
        let lastNumberIndex = currentValue.lastIndexOf(" ");
        if (lastNumberIndex === -1) {
            // Es gibt nur eine Zahl im Display
            if (currentValue.startsWith("-")) {
                display.value = currentValue.substring(1); // Remove the negative sign
            } else {
                display.value = "-" + currentValue; // Add the negative sign
            }
        } else {
            // Es gibt mehrere Zahlen im Display, das Vorzeichen der letzten Zahl ändern
            let lastNumber = currentValue.substring(lastNumberIndex + 1);
            if (lastNumber.startsWith("-")) {
                display.value = currentValue.substring(0, lastNumberIndex + 1) + lastNumber.substring(1);
            } else {
                display.value = currentValue.substring(0, lastNumberIndex + 1) + "-" + lastNumber;
            }
        }
        pressed = false;
    } else if (value == "1/") {
        display.value = addMultiplicationSign(display.value) + "1/";
        pressed = false;
    } else if (value.includes("Math") || value.includes("/100") || value.includes("**")) {
        if (value.includes("**Math")) {
            expression = expression += value;
            display.value += value.substring(7);
        } else if (value.includes("Math")) {
            if (!["+", "-", "/", "*", "(", ")"].includes(display.value.charAt(display.value.length - 1)) && display.value.length >= 1) {
                expression = expression += "*" + value;
                display.value += "*" + value.substring(5);
            } else {
                expression = expression += value;
                display.value += value.substring(5);
            }
            braces++;
        } else {
            if (!["+", "-", "/", "*"].includes(display.value.charAt(display.value.length - 1)) && display.value.length >= 1) {
                expression = expression += value;
                display.value += value;
                if (!value.includes("/100")) braces++;
            }
        }
        pressed = false;
    } else {
        expression = expression += value;
        display.value += value;
    }
    formatBraces();
}

function formatBraces() {

    document.getElementById("braces").innerHTML = "()" + "<sub>" + braces + "</sub>";
}

// Funktion zum Löschen des Displays

function deleteEventListener() {

}

function secondOperations() {
    secondND = !secondND;
    let names;
    let values;
    if (!secondND) {
        names = ["√", "sin", "cos", "tan", "ln", "log", "1/x", "e<sup>x</sup>", "x<sup>2</sup>", "x<sup>y</sup>", "|x|", "π", "e"];
        values = ["Math.sqrt(", "Math.sin(", "Math.cos(", "Math.tan(", "Math.log", "Math.log10", "1/", "Math.E**(", "**2", "**", "Math.abs", "Math.PI", "Math.E"];
    } else {
        names = ["∛", "sin<sup>-1</sup>", "cos<sup>-1</sup>", "tan<sup>-1</sup>", "sinh", "cosh", "tanh", "sinh<sup>-1</sup>", "cosh<sup>-1</sup>", "tanh<sup>-1</sup>", "2<sup>x</sup>", "x<sup>3</sup>", "x<sup>e</sup>"];
        values = ["Math.cbrt(", "Math.asin(", "Math.acos(", "Math.atan(", "Math.sinh(", "Math.cosh(", "Math.tanh(", "Math.asinh(", "Math.acosh(", "Math.atanh(", "2**", "**3", "**Math.E"];
    }
    for (let i = 3; i < 16; i++) {
        let element = document.getElementById("advanced-function" + i);
        console.log(names[i - 3]);
        element.innerHTML = names[i - 3];
        console.log(values[i - 3]);
        element.value = values[i - 3];
    }
}

function setToRad() {
    rad = !rad;
    document.getElementById("advanced-function2").innerHTML = "Rad" + "<sub>" + parseInt(rad + 0) + "</sub>";
}

function clearDisplay() {
    document.getElementById("display").value = "";
    expression = "";
    braces = 0;
    formatBraces();
}

// Funktion zum Ausführen einer Berechnung
function calculate() {
    let value = eval(expression);
    let suffix = "";
    if (rad && (expression.includes("sin") || expression.includes("cos") || expression.includes("tan"))) {
        value = value * Math.PI / 180;
        suffix = "π";
    }
    if (value.toString().includes("NaN")) {
        value = "Error!";
    }else{
        value = Math.round(value * 1000.00) / 1000.00 + suffix;
    }
    document.getElementById("display").value = value;

}