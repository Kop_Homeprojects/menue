const body = document.body,
    r = document.querySelector('#r'),
    g = document.querySelector('#g'),
    b = document.querySelector('#b'),
    outputR = document.querySelector('#outputR'),
    outputG = document.querySelector('#outputG'),
    b_out = document.querySelector('#outputB'),
    hexVal_out = document.querySelector('#hexVal');

function setColor(){
    const r_hexVal = parseInt(r.value, 10).toString(16),
        g_hexVal = parseInt(g.value, 10).toString(16),
        b_hexVal = parseInt(b.value, 10).toString(16),
        hexVal = "#" + pad(r_hexVal) + pad(g_hexVal) + pad(b_hexVal);
    body.style.backgroundColor = hexVal;
    hexVal_out.value = hexVal;
    let rgb = "";
    rgb = rgb + formatOutPut(r.value)+ "-" +formatOutPut(g.value)+"-"+formatOutPut(b.value);
    document.getElementById("rgb").innerText = rgb;
}

function formatOutPut(color){
    let value = "";
    if (color < 100){
        if (color < 10){
            value = "00"+color;
        }
        else {
            value = "0"+color;
        }
    } else {
        value = color;
    }
    return value;
}

function pad(n){
    return (n.length<2) ? "0"+n : n;
}

r.addEventListener('change', function() {
    setColor();
    outputR.value = r.value;
}, false);

r.addEventListener('input', function() {
    setColor();
    outputR.value = r.value;
}, false);

g.addEventListener('change', function() {
    setColor();
    outputG.value = g.value;
}, false);

g.addEventListener('input', function() {
    setColor();
    outputG.value = g.value;
}, false);

b.addEventListener('change', function() {
    setColor();
   b_out.value = b.value;
}, false);

b.addEventListener('input', function() {
    setColor();
   b_out.value = b.value;
}, false);