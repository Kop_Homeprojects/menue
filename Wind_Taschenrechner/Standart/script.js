"use strict";

let expression = "";
let visible_expression = "";
let currentNumber = "";
let visibleCurrentNumber = "";
const operator_signs = ["+", "-", "*", "/"]
let anzOperators = 0;


function clearExpression() {
    expression = "";
    visible_expression = "";
    document.getElementById("display").value = visible_expression;
    anzOperators = 0;
    clearNumber();
}

function clearNumber(){
    document.getElementById("display2").value = visibleCurrentNumber;
    currentNumber = "";
    visibleCurrentNumber = "";
    anzOperators = 0;
}

function removeOne(){
    currentNumber = currentNumber.substring(0, currentNumber.length-1);
    visibleCurrentNumber = visibleCurrentNumber.substring(0, visibleCurrentNumber.length-1);
    document.getElementById("display2").value = visibleCurrentNumber;
}

function addOperator(operationsign) {
    if (expression.length > 0 || currentNumber.length > 0) {
        if (operator_signs.includes(expression.charAt(expression.length - 1)) && currentNumber.length == 0) {
            if (expression.substring(expression.length-2, expression.length).includes("**")){
                expression = expression.substring(0, expression.length-2)+operationsign;
                visible_expression = visible_expression.substring(0, visible_expression.length-2)+operationsign;
            }else{
                expression = expression.substring(0, expression.length-1)+operationsign;
                visible_expression = visible_expression.substring(0, visible_expression.length-1)+operationsign;
            }
        } else {
            expression = expression + currentNumber + operationsign;
            visible_expression = visible_expression + currentNumber + operationsign;
            currentNumber = "";
            visibleCurrentNumber = "";
        }
    }
    anzOperators++;
    console.log(anzOperators);
    addToDisplay()
}


function addNumberToDisplay(number) {
    currentNumber = currentNumber + number;
    visibleCurrentNumber = visibleCurrentNumber + number;
    addToDisplay();
}

function addToDisplay() {
    document.getElementById("display").value = visible_expression;
    document.getElementById("display2").value = visibleCurrentNumber
    autoEvalExpression();
}

function autoEvalExpression() {
    if (anzOperators >= 2){
        document.getElementById("display").value =eval(visible_expression.substring(0, visible_expression.length-1))+expression.substring(expression.length-1);
        visible_expression = document.getElementById("display").value
        expression = document.getElementById("display").value
        let anzOperators = 1;
    }
}

function EvalExpression(){
    if (currentNumber.length <= 0 && operator_signs.includes(expression.charAt(expression.length-1))){
        expression = expression + 0;
        visible_expression = visible_expression + 0;
    }
    document.getElementById("display").value = visible_expression + currentNumber;
    document.getElementById("display2").value = eval(visible_expression + currentNumber);
    expression = ""+eval(expression + currentNumber);
    visible_expression = expression
    currentNumber = "";
    visibleCurrentNumber = "";
    let anzOperators = 0;
}


function devideThroghX(){
    visibleCurrentNumber = "1/"+visibleCurrentNumber;
    currentNumber = "1/"+currentNumber;
}