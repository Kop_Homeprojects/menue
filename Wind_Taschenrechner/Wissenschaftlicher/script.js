"use strict";

let evaldExpression = false;
let sc_Pressed = false;
let hyp_Pressed = false;
let expression = "";
let visible_expression = "";
let currentNumber = "";
let visibleCurrentNumber = "";
const operator_signs = ["+", "-", "*", "/"]

let brace_open = 0;
let wasOperator = false;

function addBraces(brace) {
    if (brace == ")") {
        if (brace_open > 0) {
            if (["+", "-", "*", "/"].includes(expression.charAt(expression.length - 1))) {
                visible_expression = visible_expression + visibleCurrentNumber + "+(";
                expression = expression + currentNumber + "*(";
                document.getElementById("display").value = visible_expression;
                document.getElementById("display2").value = "";
            }
        }
    } else {
        if (!["*", "+", "-", "/"].includes(expression.charAt(expression.length - 1))) {
            expression = expression + "*(";
            visible_expression = visible_expression + "*(";
        }
    }
}


function addTrigonometrikSymbol(trigonometricSymbol) {
    let number = currentNumber;
    if (currentNumber == "") {
        currentNumber = trigonometricSymbol + 0 + ")";
        visibleCurrentNumber = trigonometricSymbol.replace("Math.", "") + 0 + ")";
    } else {
        currentNumber = trigonometricSymbol + number + ")";
        visibleCurrentNumber = trigonometricSymbol.replace("Math.", "") + number.replace("Math.", "") + ")";
    }
    addToDisplay();
}

function clearDisplay() {
    expression = "";
    currentNumber = "";
    visibleCurrentNumber = "";
    visible_expression = "";
    document.getElementById("display").value = visible_expression;
    document.getElementById("display2").value = visibleCurrentNumber;
}

function addOperator(operationsign) {
    if (expression.length > 0 || currentNumber.length > 0) {
        if (operator_signs.includes(expression.charAt(expression.length - 1)) && currentNumber.length == 0) {
            if (expression.substring(expression.length-2, expression.length).includes("**")){
                expression = expression.substring(0, expression.length-2)+operationsign;
                visible_expression = visible_expression.substring(0, visible_expression.length-2)+operationsign;
            }else{
                expression = expression.substring(0, expression.length-1)+operationsign;
                visible_expression = visible_expression.substring(0, visible_expression.length-1)+operationsign;
            }
        } else if (evaldExpression) {
            document.getElementById("display").value = document.getElementById("display2").value + operationsign;
            document.getElementById("display2").value = "";
            evaldExpression = false;
        } else {
            expression = expression + currentNumber + operationsign;
            visible_expression = visible_expression + currentNumber + operationsign;
            currentNumber = "";
            visibleCurrentNumber = "";
            evaldExpression = false;
        }
    }
    addToDisplay()
}

function addNumberToDisplay(number) {
    currentNumber = currentNumber + number;
    visibleCurrentNumber = visibleCurrentNumber + number;
    addToDisplay();
}

function addToDisplay() {
    document.getElementById("display").value = visible_expression;
    document.getElementById("display2").value = visibleCurrentNumber;
}

function evalExpression() {
    if (currentNumber.length <= 0 && operator_signs.includes(expression.charAt(expression.length-1))){
        expression = expression + 0;
        visible_expression = visible_expression + 0;
    }
    document.getElementById("display").value = visible_expression + visibleCurrentNumber + "=";
    document.getElementById("display2").value = eval(expression + currentNumber);
    evaldExpression = true;
}


function addArcFunctions() {
    const trigonmetricalFunctions = document.getElementsByClassName("button operator trigonomie");
    for (let i = 0; i < 4; i++) {
        if (!["2<sup>nd</sup>"].includes(trigonmetricalFunctions.item(i).innerHTML) && !trigonmetricalFunctions.item(i).innerHTML.match("^a")) {
            sc_Pressed = true;
            trigonmetricalFunctions.item(i).innerHTML = "a" + trigonmetricalFunctions.item(i).innerHTML.replace("Math.", "");
            trigonmetricalFunctions.item(i).value = "Math.a" + trigonmetricalFunctions.item(i).value.replace("Math.", "");
        } else if (!["2<sup>nd</sup>"].includes(trigonmetricalFunctions.item(i).innerHTML)) {
            sc_Pressed = false;
            trigonmetricalFunctions.item(i).innerHTML = trigonmetricalFunctions.item(i).innerHTML.replace("a", "");
            trigonmetricalFunctions.item(i).value = "Math." + trigonmetricalFunctions.item(i).value.replace("Math.a", "");
        }
    }
    for (let i = 4; i < trigonmetricalFunctions.length; i++) {
        if (!["hyp"].includes(trigonmetricalFunctions.item(i).innerHTML) && !trigonmetricalFunctions.item(i).innerHTML.match("^a")) {
            trigonmetricalFunctions.item(i).innerHTML = "a" + trigonmetricalFunctions.item(i).innerHTML.replace("Math.", "");
            trigonmetricalFunctions.item(i).value = trigonmetricalFunctions.item(i).value.substring(0, 2) + "Math.a" + trigonmetricalFunctions.item(i).value.substring(7)
        } else if (!["hyp"].includes(trigonmetricalFunctions.item(i).innerHTML)) {
            trigonmetricalFunctions.item(i).innerHTML = trigonmetricalFunctions.item(i).innerHTML.replace("a", "");
            trigonmetricalFunctions.item(i).value = "Math." + trigonmetricalFunctions.item(i).value.replace("Math.a", "");
        }
    }
}

function addHypFunctions() {
    const trigonmetricalFunctions = document.getElementsByClassName("button operator trigonomie");
    let endIndex = 3
    if (sc_Pressed) {
        endIndex = 4;
    }
    for (let i = 1; i < 4; i++) {
        if (!trigonmetricalFunctions.item(i).innerHTML.includes("h")) {
            trigonmetricalFunctions.item(i).innerHTML = trigonmetricalFunctions.item(i).innerHTML.substring(0, endIndex) + "h" + trigonmetricalFunctions.item(i).innerHTML.substring(endIndex);
            trigonmetricalFunctions.item(i).value = trigonmetricalFunctions.item(i).value.substring(0, endIndex+5) + "h" + trigonmetricalFunctions.item(i).value.substring(endIndex+5);
        } else {
            trigonmetricalFunctions.item(i).innerHTML = trigonmetricalFunctions.item(i).innerHTML.replace("h", "");
            trigonmetricalFunctions.item(i).value = trigonmetricalFunctions.item(i).value.replace("h", "");
        }
    }
}

function addPower(x, y){
    if (x == 0 && y != 0){
        if (currentNumber.length == 0){
            currentNumber = 0 ** y;
            visibleCurrentNumber = currentNumber;
        }else{
            currentNumber = currentNumber ** y
            visibleCurrentNumber = currentNumber;
        }
    }else if (x == "10" && y != 0){
        if (currentNumber.length == 0){
            x ** 0
        }else{
            x ** currentNumber;
        }
    }else if (x == 0 && y == 0){
        if (currentNumber.length == 0 && expression.length > 0){
            visible_expression = "(" + visible_expression + ")**";
            expression = "(" + expression + ")**";
        } else if (expression.length <= 0 && currentNumber.length <= 0){
            visible_expression = "0**";
            expression = "0**";
        } else {
            visible_expression = visible_expression +visibleCurrentNumber+ "**";
            expression = expression+currentNumber+"**";
            visibleCurrentNumber = "";
            currentNumber = "";
        }
    }
    addToDisplay();
}

function devideThroghX(){
    visibleCurrentNumber = "1/"+visibleCurrentNumber;
    currentNumber = "1/"+currentNumber;
}