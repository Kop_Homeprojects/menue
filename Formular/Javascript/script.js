document.addEventListener("DOMContentLoaded", () => {
    /*
    Adds all EventListeners to all sort-init buttons
     */
    document.getElementById("init").addEventListener("click", () => {
        init(["Mustermann", "Max", 1234, "Musterstadt", "mustermann@gmail.com", "2000-12-24", "2BHIF"], [true, false, false], [true, true, true, false])
    });
    //document.getElementById("rollback").addEventListener("click", () => rollBack(personen));
    loadLocalStorage(true);
})

// ads all Eventlisteners

// Global Variables
let personen = [] // Person array, where the action submit will be stored
let counter = 0;
let timestampPressed = false; // false -> off true -> on
let NameSortPressed = false;  // false -> off true -> on
let PLZSortPressed = false;   // false -> off true -> on

// CheckForm function
function checkForm(usedFor) {
    /*
    checks the complete formular for possible errors, via
    regualar expressions
     */
    let form = document.getElementById('dataform');
    let errors = [];
    let formdata = new FormData(form);
    let lastName = formdata.get("name");
    let firstName = formdata.get("vorname");
    let plz = formdata.get("PLZ");
    let location = formdata.get("Ort");
    let email = formdata.get("email");
    let birthDate = formdata.get("datum");
    checkItems(lastName, "lastName", errors);
    checkItems(firstName, "firstName", errors);
    checkPLZ(plz, errors);
    checkItems(location, "location", errors);
    checkEmail(email, errors);
    checkDate(birthDate, errors);
    printErrors(errors);
    if (errors.length < 1) {
        let values = [lastName, firstName, plz, location, checkGeschlecht(), email, formdata.get("datum"), formdata.get("klasse"), checkHobbies()];
        if (usedFor != "update") {
            print(values, false);
        } else {
            return values;
        }
        clear();
    }
}

function print(values, comesfrom) {
    //makeBackUp(personen);
    personen[personen.length] = new Person(values, comesfrom);
    if (!comesfrom) {
        saveArrayToLocaleStorage(personen);
    }
    if (document.getElementById("tableHead") == null) {
        printData(personen[personen.length - 1], 1);
    } else {
        printData(personen[personen.length - 1], 0);
    }
    counter++;
}

/*
  SortFunctions:
 */
function sortName() {
    if (NameSortPressed == false) {
        for (let j = 0; j < personen.length - 1; j++) {
            for (let i = j + 1; i < personen.length; i++) {
                if (personen[j].name.localeCompare(personen[i].name) > 0) {
                    let temp = personen[j];
                    personen[j] = personen[i];
                    personen[i] = temp;
                }
            }
        }
        NameSortPressed = true;
    } else {
        personen.reverse();
    }
    printSortedData(personen);
}

function sortPLZ() {
    if (PLZSortPressed == false) {
        for (let j = 0; j < personen.length - 1; j++) {
            for (let i = j + 1; i < personen.length; i++) {
                if (personen[j].plz.localeCompare(personen[i].plz) > 0) {
                    let temp = personen[j];
                    personen[j] = personen[i];
                    personen[i] = temp;
                }
            }
        }
        PLZSortPressed = true;
    } else {
        personen.reverse();
    }
    printSortedData(personen);
}

function sortTimeStamp() {
    if (timestampPressed == 0) {
        for (let j = 0; j < personen.length - 1; j++) {
            for (let i = j + 1; i < personen.length; i++) {
                if (personen[j].timestamp > personen[i].timestamp) {
                    let temp = personen[j];
                    personen[j] = personen[i];
                    personen[i] = temp;
                }
            }
        }
        timestampPressed = true;
    } else {
        personen.reverse();
    }
    printSortedData(personen);
}

/*
  checkFunctions:
*/
function checkItems(item, action, errors) {
    /*
    Checks the firstName, lastName and location for possible errors
     */
    if (!item.match("^[A-ZÄÖÜ][a-zäöü]{2}")) {
        if (action == "lastName") {
            errors[0] = "Der erste Buchstabe des Namens muss groß sein."
        } else if (action == "firstName") {
            errors[1] = "Der erste Buchstabe des Vornamens muss groß sein."
        } else if (action == "location") {
            errors[3] = "Der erste Buchstabe des Ortes muss groß sein."
        }
    }
}

function checkPLZ(plz, errors) {
    if (plz < 1010 || plz > 9999) {
        errors[2] = "Die Postleitzahl muss zwischen 1010 und 9999 liegen."
    }
}

function checkEmail(email, errors) {
    if (!email.match("[A-Za-z0-9]{4,}" + "@[A-Za-z0-9.-]{3,}." + "[A-Z|a-z]{3,6}")) {
        errors[4] = "Die Email Addresse ist fälschlich.";
    }
}

function checkDate(datum, errors) {
    let date = "" + new Date().getFullYear();
    if ((parseInt(datum) > (parseInt(date) - 14))) {
        errors[5] = "Falsches geburtsdatum, du musst mind 14 Jahre alt sein! Oder inkorrektes Datum!";
    } else if (datum == "") {
        errors[5] = "Bitte gib ein Datum ein!";
    }
}

function deleteAll() {
    /*
    DeleteAllButtun Listener -> deletes Whole Table
     */
    //makeBackUp(personen);
    document.getElementById("tbody").remove();
    document.getElementById("tableHead").remove();
    personen = [];
    saveArrayToLocaleStorage(personen);
}

function printSortedData(persons) {
    document.getElementById("tbody").remove();
    let tbody = document.createElement("tbody");
    tbody.id = "tbody";
    document.getElementById("table").appendChild(tbody);
    for (let i = 0; i < persons.length; i++) {
        tbody.appendChild(createTableRow(persons[i]));
    }
}

function createRowButtons(tr2) {
    // Button und Inhalte erzeugen!
    let th1 = document.createElement("td");
    let th2 = document.createElement("td");
    let deleteButton = document.createElement("button");
    let updateButton = document.createElement("button");
    // namen setzten
    deleteButton.appendChild(document.createTextNode("Del"))
    updateButton.appendChild(document.createTextNode("Uptd"))
    // Event Listeners
    deleteButton.addEventListener("click", deleteRow);
    updateButton.addEventListener("click", updateValues)

    th1.append(deleteButton)
    th2.append(updateButton)
    tr2.append(th1);
    tr2.append(th2)
    return tr2;
}

function printData(person, build) {
    let headers = ["Name", "Vorname", "Postleitzahl", "Ort", "Geschlecht", "Email", "Geburtsdatum", "Klasse", "Hobbies", "Timestamp", "Del", "Uptd"]
    let tr1 = document.createElement("tr");
    if (build == 1) {
        let thead = document.createElement("thead");
        thead.id = "tableHead";
        for (let i = 0; i < headers.length; i++) {
            let th = document.createElement("td");
            if (headers[i] != "Del") {
                th.appendChild(document.createTextNode(headers[i]));
                if (headers[i] == "Name" || headers[i] == "Postleitzahl" || headers[i] == "Timestamp") {
                    let button;
                    if (headers[i] == "Name") {
                        button = document.createElement("button");
                        button.appendChild(document.createTextNode("SortName"));
                        button.addEventListener("click", sortName);
                    } else if (headers[i] == "Postleitzahl") {
                        button = document.createElement("button");
                        button.appendChild(document.createTextNode("SortPLZ"));
                        button.addEventListener("click", sortPLZ);
                    } else if (headers[i] == "Timestamp") {
                        button = document.createElement("button")
                        button.appendChild(document.createTextNode("SortTimestamp"));
                        button.addEventListener("click", sortTimeStamp);
                    }
                    button.id = "sortButtons"
                    th.append(button);
                }
            }
            if (headers[i] == "Del") {
                let deleteAllButton = document.createElement("button");
                deleteAllButton.appendChild(document.createTextNode("DelALL"));
                deleteAllButton.id = "delAllButton";
                deleteAllButton.className = "delButton";
                deleteAllButton.addEventListener("click", deleteAll);
                th.appendChild(deleteAllButton);
            }
            tr1.appendChild(th);
        }
        thead.appendChild(tr1);
        document.getElementById("table").appendChild(thead);
        if (document.getElementById("tbody") == null) {
            let thbody = document.createElement("tbody")
            thbody.id = "tbody";
            document.getElementById("tableHead").parentElement.appendChild(thbody)
        }
    }
    if (document.getElementById("tbody") == null) {
        let tbody = document.createElement("tbody");
        tbody.id = "tbody";
        document.getElementById("table").appendChild(tbody);
    }
    let tbody = document.getElementById("tbody");
    tbody.appendChild(createTableRow(person));
    document.getElementById("table").appendChild(tbody);
}

function createTableRow(person) {
    let tr2 = document.createElement("tr");


    for (let i = 0; i <= 9; i++) {
        let th = document.createElement("td");
        switch (i) {
            case 0:
                th.appendChild(document.createTextNode(person.name));
                break;
            case 1:
                th.appendChild(document.createTextNode(person.vorname));
                break;
            case 2:
                th.appendChild(document.createTextNode(person.plz))
                break;
            case 3:
                th.appendChild(document.createTextNode(person.ort))
                break;
            case 4:
                th.appendChild(document.createTextNode(person.geschlecht));
                break;
            case 5:
                th.appendChild(document.createTextNode(person.email));
                break;
            case 6:
                th.appendChild(document.createTextNode(person.datum));
                break;
            case 7:
                th.appendChild(document.createTextNode(person.klasse));
                break;
            case 8:
                th.appendChild(document.createTextNode(person.hobbies));
                break;
            case 9:
                th.appendChild(document.createTextNode(person.grafikTimeStamp));
                break;
        }
        tr2.append(th)
    }
    tr2 = createRowButtons(tr2)
    return tr2;
}

function deleteRow(event) {
    personen.splice(event.currentTarget.parentElement.parentElement.rowIndex - 1, event.currentTarget.parentElement.parentElement.rowIndex)
    event.currentTarget.parentElement.parentElement.remove();
    if (document.getElementById("table").lastElementChild.firstChild == null) {
        document.getElementById("table").firstElementChild.remove()
        document.getElementById("table").lastElementChild.remove();
    }
    saveArrayToLocaleStorage(personen);
}

function getPerson(name, vorname, plz) {
    for (let i = 0; i < personen.length; i++) {
        if ((personen[i].plz == plz && personen[i].name == name && personen[i].vorname == vorname)) {
            return personen[i]
        }
    }
}

function actiDeactivateButtons(b1, b2) {
    document.getElementById("init").disabled = b1;
    document.getElementById("submit").disabled = b2;
}

function updateValues(event) {
    let row = event.currentTarget.parentElement.parentElement;
    let element = event.currentTarget.parentElement.parentElement.firstChild;
    actiDeactivateButtons(true, true);
    let person = getPerson(element.firstChild.nodeValue, element.nextSibling.firstChild.nodeValue, element.nextSibling.nextSibling.firstChild.nodeValue);
    let geschlecht = ["Männlich", "Weiblich", "Divers"];
    for (let j = 0; j < 3; j++) {
        geschlecht[j] = geschlecht[j] == person.geschlecht;
    }
    let hobbieArray = []
    for (let j = 0; j <= person.hobbies.length; j++) {
        let l;
        for (l = 0; l < 4; l++) {
            if (document.getElementById("formdata-hobbie" + l).value == person.hobbies[j]) {
                hobbieArray.push(true);
                break;
            }
        }
        if (l + 1 == 4) {
            hobbieArray.push(false);
        }
    }
    let data = [person.name, person.vorname, person.plz, person.ort, person.email, person.datum, person.klasse];
    init(data, geschlecht, hobbieArray);

    let button = document.createElement("button");
    button.appendChild(document.createTextNode("Update"));
    let span = document.getElementById("updateButtonContainer");
    button.addEventListener("click", function () {
        updatePerson(person, element, row)
    })
    row.bgColor = "#539341"
    span.append(button);
}

function updatePerson(person, ele, row) {
    //makeBackUp(person);
    let daten = checkUpdate(row);
    if (daten != null) {
        let elem = ele;
        person.name = daten[0];
        elem.firstChild.nodeValue = person.name;
        elem = elem.nextSibling;
        person.vorname = daten[1];
        elem.firstChild.nodeValue = person.vorname;
        elem = elem.nextSibling;
        person.plz = daten[2];
        elem.firstChild.nodeValue = person.plz;
        elem = elem.nextSibling;
        person.ort = daten[3];
        elem.firstChild.nodeValue = person.ort;
        elem = elem.nextSibling;
        person.geschlecht = daten[4];
        elem.firstChild.nodeValue = person.geschlecht;
        elem = elem.nextSibling;
        person.email = daten[5];
        elem.firstChild.nodeValue = person.email
        elem = elem.nextSibling;
        person.datum = daten[6];
        elem.firstChild.nodeValue = person.datum;
        elem = elem.nextSibling;
        person.klasse = daten[7];
        elem.firstChild.nodeValue = person.klasse;
        elem = elem.nextSibling;
        person.hobbies = daten[8];
        elem.firstChild.nodeValue = person.hobbies;
        elem = elem.nextSibling;
        person.timestamp = new Date();
        elem.firstChild.nodeValue = (person.grafikTimeStamp);
    }

    document.getElementById("updateButtonContainer").firstElementChild.remove();

    saveArrayToLocaleStorage(personen);

    actiDeactivateButtons(false, false);
}

function checkHobbies() {
    let hobbies = [];
    let index = 0;
    for (let i = 0; i < 4; i++) {
        if (document.getElementById("formdata-hobbie" + i).checked == true) {
            hobbies[index] = document.getElementById("formdata-hobbie" + i).value;
            index++;
        }
    }
    return hobbies;
}

function checkGeschlecht() {
    let name = "";
    for (let i = 0; i < 3; i++) {
        if (document.getElementById("formdata-geschlecht" + i).checked == true) {
            name = document.getElementById("formdata-geschlecht" + i).value;
        }
    }
    return name;
}

function checkUpdate(row) {
    let daten = checkForm("update");
    if (daten != undefined) {
        row.bgColor = "";
        clear();
    }
    return daten;
}

class Person {
    constructor(data, localStorage) {
        let daten = [];
        for (const key in data) {
            daten.push(data[key]);
        }
        this.name = daten[0];
        this.vorname = daten[1];
        this.plz = daten[2];
        this.ort = daten[3];
        this.geschlecht = daten[4];
        this.email = daten[5];
        this.datum = daten[6];
        this.klasse = daten[7];
        this.hobbies = daten[8];
        if (!localStorage) {
            this.timestamp = new Date();
            // Grafikal Timestmap will be shown in the Table!
            this.grafikTimeStamp = this.timestamp.toLocaleDateString() + " " + this.timestamp.toTimeString().substring(0, 8);
        } else {
            this.timestamp = daten[9];
            this.grafikTimeStamp = daten[10];
        }
    }
}

// setzt ein Interval für die Uhr
function printErrors(errors) {
    document.getElementById("name-error").firstChild.nodeValue = errors[0];
    document.getElementById("vorname-error").firstChild.nodeValue = errors[1];
    document.getElementById("PLZ-error").firstChild.nodeValue = errors[2];
    document.getElementById("ort-error").firstChild.nodeValue = errors[3];
    document.getElementById("email-error").firstChild.nodeValue = errors[4];
    document.getElementById("datum-error").firstChild.nodeValue = errors[5];
}
function clear() {
    /*
    clears the hole form after submitting a correct form
     */
    init(["", "", "", "", "", "", ""], [false, false, false], [false, false, false, false]);
}
function init(data, geschlecht, hobbies) {
    document.getElementById("formdata-name").value = data[0];
    document.getElementById("formdata-vorname").value = data[1];
    document.getElementById("formdata-PLZ").value = data[2];
    document.getElementById("formdata-ort").value = data[3];
    document.getElementById("formdata-geschlecht0").checked = geschlecht[0];
    document.getElementById("formdata-geschlecht1").checked = geschlecht[1];
    document.getElementById("formdata-geschlecht2").checked = geschlecht[2];
    document.getElementById("formdata-email").value = data[4];
    document.getElementById("formdata-datum").value = data[5];
    document.getElementById("formdata-hobbie0").checked = hobbies[0];
    document.getElementById("formdata-hobbie1").checked = hobbies[1];
    document.getElementById("formdata-hobbie2").checked = hobbies[2];
    document.getElementById("formdata-hobbie3").checked = hobbies[3];
    document.getElementById("formdata-klasse").value = data[6];
}