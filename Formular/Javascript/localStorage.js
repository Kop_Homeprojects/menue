// localstorage
let backupPersonen = [];

function loadLocalStorage(printData, i) {
    if (printData == true) {
        for (let i = 0; i < localStorage.length; i++) {
            let string = localStorage.getItem(localStorage.key(i))
            let data = JSON.parse(string);
            print(data, true);
        }
    } else {
        let string = localStorage.getItem(localStorage.key(i))
        let data = JSON.parse(string);
        return new Person(data, true);
    }
}

function saveArrayToLocaleStorage(members) {
    localStorage.clear();
    for (let i = 0; i < members.length; i++) {
        localStorage.setItem(personen[i].name,
            JSON.stringify({
                name: personen[i].name,
                vorname: personen[i].vorname,
                plz: personen[i].plz,
                ort: personen[i].ort,
                geschlecht: personen[i].geschlecht,
                email: personen[i].email,
                datum: personen[i].datum,
                Klasse: personen[i].klasse,
                hobbies: personen[i].hobbies,
                timestamp: personen[i].timestamp,
                grafiktimestamp: personen[i].grafikTimeStamp
            }));
    }
}
/**
 * Will be used Later
 *
function makeBackUp(){
    backupPersonen = [];
    for (let i = 0; i < localStorage.length; i++) {
        backupPersonen.push(loadLocalStorage(false, i));
    }
    console.log(backupPersonen);
}
function rollBack() {
    document.getElementById("table").lastElementChild.remove();
    document.getElementById("table").firstElementChild.remove();
    for (let i = 0; i < backupPersonen.length; i++) {
        print(backupPersonen[i].toArray(), true);
    }

}*/