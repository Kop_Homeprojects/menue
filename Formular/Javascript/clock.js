class Clock {
    constructor(el){
        this.clockEl = el;
        this.UI = {};
        this.initializeClock();
    }

    updateClock = () => {
        // GETTING TIME
        const date = new Date();
        // const date = now.getDate();
        const seconds = (date.getSeconds() + date.getMilliseconds() / 1000) / 60 * 360;
        const minutes = date.getMinutes() * 6;
        const hours = date.getHours() * 6;
        // UI Update
        this.UI.second.style.transform = `rotate(${seconds}deg)`;
        this.UI.minute.style.transform = `rotate(${minutes}deg)`;
        this.UI.hour.style.transform = `rotate(${hours}deg)`;
        requestAnimationFrame(this.updateClock)
    }

    initializeClock() {
        this.UI.second = this.clockEl.querySelector('.hand--second');
        this.UI.minute = this.clockEl.querySelector('.hand--minute');
        this.UI.hour = this.clockEl.querySelector('.hand--hour');
        requestAnimationFrame(this.updateClock)
    }
}


const clocks = document.querySelectorAll('.clock');
clocks.forEach(el => new Clock(el))
